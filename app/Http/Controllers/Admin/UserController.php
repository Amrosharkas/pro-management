<?php

namespace App\Http\Controllers\Admin;
use App\User;
use Illuminate\Http\Request;
use Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function getUsers(){

		$users = User::get();
		return view('admin.get_users',compact('users'));

    }
    public function userDetails($id){

		$user = User::find($id);
		return view('admin.user_details',compact('user'));

    }

    public function save(Request $request){
		$data = $request->input();
 		$data['password'] = Hash::make($data['password']);		
    	if($request->input('id') ==""){
			$user = User::create($data);
		}else{
			$user = User::find($request->input('id'))->update($data);
		}
		return 'Success';

    }
}
