<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
	// Admin Routes
Route::group(['middleware' => 'auth'], function () {
	Route::get('/', function () {
		return view('admin.calendar');
	});
	Route::get('admin/users', function () {
		return view('admin.users');
	});
	
	Route::get('admin/getUsers', [
		'uses' => 'Admin\UserController@getUsers'
	]);
	
	Route::get('admin/user/{id}', [
		'uses' => 'Admin\UserController@userDetails'
	]);
	
	Route::get('admin/user/add', function () {
		return view('admin.user_details');
	});
	
	Route::post('/admin/user/save', [
		'uses' => 'Admin\UserController@save'
	]);
});

	// Authentication routes...
	Route::get('/auth/login', 'Auth\AuthController@getLogin');
	Route::post('/auth/login', 'Auth\AuthController@postLogin');
	Route::get('/auth/logout', 'Auth\AuthController@logout');
	
	// Registration routes...
	Route::get('/auth/register', 'Auth\AuthController@getRegister');
	Route::post('/auth/register', 'Auth\AuthController@postRegister');

	// Password reset link request routes...
	Route::get('password/email', 'Auth\PasswordController@getEmail');
	Route::post('password/email', 'Auth\PasswordController@postEmail');

	// Password reset routes...
	Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
	Route::post('password/reset', 'Auth\PasswordController@postReset');
});



Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
});
