<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->nullable();
            $table->integer('stage_dependency')->nullable();
            $table->string('stage_name')->nullable();
            $table->string('completeion_status')->nullable();
            $table->date('est_start_date')->nullable();
            $table->date('act_start_date')->nullable();
            $table->date('est_end_date')->nullable();
            $table->date('act_end_date')->nullable();
            $table->string('stage_remarks')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stages');
    }
}
